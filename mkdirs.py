import os, sys, subprocess, re

def main(COURSENAME, COURSEURL, COURSELEN, PHPSESSID):
	os.system(f'rm -rf "{COURSENAME}"')
	os.mkdir(COURSENAME)

	for i in open(".tmp/lessons.txt"):
		lineNo = i.split(";")[0].strip()

		print(f"Making directory {lineNo} / {COURSELEN}...")

		url = i.split(";")[1].strip()
		lessonName = subprocess.check_output(f'curl -s --cookie PHPSESSID={PHPSESSID} {url} | grep "<title>"', shell=True)
		dirName = f"{lineNo}. {lessonName.decode('utf-8')[11:-25]}"

		allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,-_()[]{}!£$%^&`¬¦=+;'@#~ "

		for j in dirName:
			if j not in allowedChars:
				dirName = dirName.replace(j, "_")

		os.mkdir(f"./{COURSENAME}/{dirName.strip()}")
