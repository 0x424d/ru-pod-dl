if [ $# -ne 2 ];
then
    echo "[!] Error: $# != 2"
    exit 1
fi

while IFS=";" read -ra p;
do
    echo "Scraping for media sources from lesson ${p[0]} / $2..."

    sourcesV=`curl ${p[1]} -s --cookie "PHPSESSID=$1" | grep -E 'mdn.illops.net/russianpod101/.+\.(m[p4][34v])'`
    sourcesP=`curl ${p[1]} -s --cookie "PHPSESSID=$1" | grep -E '/pdfs/.+\.pdf'`

    if [ -n "$sourcesV" ];
    then
        echo "Found audio or video sources for this lesson!"
    else
        echo "Could not find audio or video sources for this lesson."
    fi

    for i in ${sourcesV[@]};
    do
        if [[ $i == src* ]];
        then
            echo "${p[0]};${i:5:-1}" >> .tmp/media.tmp
        fi
    done

    if [ -n "$sourcesP" ];
    then
        echo "Found PDF sources for this lesson!"
    else
        echo "Could not find PDF sources for this lesson."
    fi

    for i in ${sourcesP[@]};
    do
        if [[ $i == href* ]];
        then
			if [[ $i != *www.innovativelanguage.com* ]];
			then
				echo "${p[0]};https://www.russianpod101.com${i:6:-1}" >> .tmp/media.tmp
			else
				if [ ! -f .tmp/checklist.pdf ];
				then
					echo "Found checklist.pdf!"
					curl ${i:6:-1} -L -s --cookie "PHPSESSID=$1" --output ".tmp/checklist.pdf"
				fi
			fi
		fi
    done

done < .tmp/lessons.txt
