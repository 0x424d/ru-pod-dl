if [ $# -ne 2 ];
then
    echo '[!] Error: $# != 2'
    exit 1
fi

curl --cookie "PHPSESSID=$1" -s $2 | grep "/lesson/.*" -o > .tmp/lessons.txt
