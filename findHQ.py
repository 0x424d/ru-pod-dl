import os
from collections import Counter

def main():
	with open(".tmp/media.tmp") as f, open(".tmp/media.txt", "w") as g:
		fLines = f.readlines()

		# eliminate duplicates
		fLines = list(Counter(i for i in fLines if i))

		for c, i in enumerate(fLines):
			if i.endswith(".m4v\n"):
				if fLines[c+1].endswith(".mp4\n"):
					continue
			g.write(i)

	os.system("rm -f .tmp/media.tmp")
