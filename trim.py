import os

def prepend(fh, gh):
	for i in fh:
		line = f"https://www.russianpod101.com{i}"
		gh.write(line)

def trim(fh, gh):
	next(fh)
	next(fh)
	lineNo = 0
	for i in fh:
		lineNo += 1
		line = i[:-3]
		line = f"{lineNo};{line}\n"
		gh.write(line)

def cleanup():
	os.system("rm -f .tmp/lessons.txt .tmp/lessons_prepended.tmp")
	os.system("mv .tmp/lessons_fin.tmp .tmp/lessons.txt")

def main():
	with open(".tmp/lessons.txt") as f, open(".tmp/lessons_prepended.tmp", "w+") as g, open(".tmp/lessons_fin.tmp", "w") as h:
		prepend(f, g)
		f.seek(0)
		g.seek(0)
		trim(g, h)
		cleanup()
