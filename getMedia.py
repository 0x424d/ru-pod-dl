import os

def getfstring(filename):
	# %1 = PHPSESSID, %2 = dlURL, %3 = wd, %4 = ext
	if filename.strip().endswith("_recordingscript.pdf"):
		return '--cookie "PHPSESSID=%1" --output "transcript.pdf" --progress-bar -L %2'

	elif filename.strip().endswith(".pdf"):
		return '--cookie "PHPSESSID=%1" --output "notes.pdf" --progress-bar -L %2'

	elif filename.strip().endswith(".mp4") or filename.strip().endswith(".m4v"):
		print(f'Copying checklist.pdf to {os.getcwd().split("/")[-1]}...')
		os.system("cp ../../.tmp/checklist.pdf ./checklist.pdf")
		print("Success!")
		return '--cookie "PHPSESSID=%1" --output "%3.%4" --progress-bar -L %2'

	elif filename.strip().endswith(".mp3"):
		count = 0
		countIntToStr = ["lesson", "dialogue", "vocab"]
		for i in os.listdir():
			if i.endswith(".mp3"):
				count += 1
		if count == 0:
			print(f'Copying checklist.pdf to {os.getcwd().split("/")[-1]}...')
			os.system("cp ../../.tmp/checklist.pdf ./checklist.pdf")
			print("Success!")
		return f'--cookie "PHPSESSID=%1" --output "%3 {countIntToStr[count]}.%4" --progress-bar -L %2'

	else:
		fstring = '--cookie "PHPSESSID=%1" --progress-bar -L -O %2'

def main(COURSENAME, COURSEURL, COURSELEN, PHPSESSID):
	dirsDict = {}

	for i in os.listdir(f"./{COURSENAME}"):
		dirsDict[i.split(".")[0]] = i

	os.chdir(COURSENAME)

	with open("../.tmp/media.txt") as f:
		for i in f:
			dirNo = i.split(";")[0]
			dlURL = i.split(";")[1]
			filename = i.split("/")[4].split("?")[0]
			ext = filename.split(".")[-1].strip()
			wd = dirsDict[dirNo].strip()

			os.chdir(wd)

			fstring = getfstring(filename).replace("%1", PHPSESSID).replace("%2", dlURL).replace("%3", wd).replace("%4", ext)
			
			print(f"Downloading {filename.strip()} into directory {dirNo} / {COURSELEN}...")
			
			os.system(f"curl {fstring}")
			print("Success!")
			print("")
			
			os.chdir("..")
