import os, subprocess
import trim, mkdirs, findHQ, getMedia

class colours:
	RESET = "\033[0m"
	RED = "\033[93m"
	GREEN = "\033[92m"
	BLUE = "\033[94m"

# TODO: take PHPSESSID and COURSEURL from the command line
PHPSESSID = "tiorf9p7m62m866c8k6hc5e5q5"
COURSEURL = "https://www.russianpod101.com/lesson-library/mastering-level-1-russian/"
COURSENAME = COURSEURL.split("/")[4].replace("-", " ").title()

# ===================================================================================================================

def preprocessing():
	print(f"{colours.BLUE}Resetting...{colours.RESET}")
	os.system("bash reset.sh")
	print(f"{colours.GREEN}Reset complete!\n{colours.RESET}")

	print("=======================================================================================\n")

	os.mkdir(".vars")
	os.mkdir(".tmp")
	os.system("touch .tmp/media.txt")
	os.system("touch .tmp/media.tmp")
	os.system("touch .tmp/checklist.tmp")

# ===================================================================================================================

def processing():
	try:
		print(f"{colours.BLUE}Curling course page for lessons...{colours.RESET}")
		subprocess.run(f"bash curlPage.sh {PHPSESSID} {COURSEURL}".split(" "))
		print(f"{colours.GREEN}Curling complete!\n{colours.RESET}")

		print("=======================================================================================\n")

		print(f"{colours.BLUE}Trimming lessons.txt...{colours.RESET}")
		trim.main()
		print(f"{colours.GREEN}Trimming complete!\n{colours.RESET}")

		print("=======================================================================================\n")

		# get course length for later
		with open(".tmp/lessons.txt") as f:
			COURSELEN = f.readlines()[-1].split(";")[0].strip()

		print(f"{colours.BLUE}Making directories...{colours.RESET}")
		mkdirs.main(COURSENAME, COURSEURL, COURSELEN, PHPSESSID)
		print(f"{colours.GREEN}Directories made!\n{colours.RESET}")

		print("=======================================================================================\n")

		print(f"{colours.BLUE}Grabbing media URLs...{colours.RESET}")
		subprocess.run(f"bash grabMediaURLs.sh {PHPSESSID} {COURSELEN}".split(" "))
		print(f"{colours.GREEN}Grabbed!\n{colours.RESET}")

		print("=======================================================================================\n")

		print(f"{colours.BLUE}Finding highest quality videos and eliminating URL duplicates...{colours.RESET}")
		findHQ.main()
		print(f"{colours.GREEN}Done!\n{colours.RESET}")

		print("=======================================================================================\n")

		print(f"{colours.BLUE}Downloading files...{colours.RESET}")
		getMedia.main(COURSENAME, COURSEURL, COURSELEN, PHPSESSID)
		print(f"{colours.GREEN}Downloads complete!\n{colours.RESET}")
	except subprocess.CalledProcessError:
		print("[!] If you're seeing this, 0x424d seriously messed up. Aborting.")
		quit()

# ===================================================================================================================

def postprocessing():
	os.system("rm -rf .vars .tmp")

# ===================================================================================================================

preprocessing()
processing()
postprocessing()
